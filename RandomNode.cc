#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "randomMessage_m.h"

using namespace omnetpp;

class RandomNode : public cSimpleModule
{
  protected:
    virtual RandomMessage *generateMessage();
    virtual void forwardMessage(RandomMessage *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(RandomNode);

void RandomNode::initialize()
{
    // Module 0 sends the first message
    if (getIndex() == 0) {
        // Boot the process scheduling the initial message as a self-message.
        RandomMessage *msg = generateMessage();
        scheduleAt(0.0, msg);
    }
}
void RandomNode::handleMessage(cMessage *msg)
{
    RandomMessage *ttmsg = check_and_cast<RandomMessage *>(msg);
    if (ttmsg->getDestination() == getIndex()) {
        // Message arrived.
        EV << "Message " << ttmsg << " arrived after " << ttmsg->getHopCount() << " hops.\n";
        bubble("ARRIVED, starting new one!");
        delete ttmsg;
        // Generate another one.
        EV << "Generating another message: ";
        RandomMessage *newmsg = generateMessage();
        EV << newmsg << endl;
        forwardMessage(newmsg);
    }
    else {
        // We need to forward the message.
        forwardMessage(ttmsg);
    }
}
RandomMessage *RandomNode::generateMessage()
{
    // Produce source and destination addresses.
    int src = getIndex();  // our module index
    int n = getVectorSize();  // module vector size
    int dest = intuniform(0, n-2);
    if (dest >= src)
        dest++;
    char msgname[20];
    sprintf(msgname, "tic-%d-to-%d", src, dest);
    // Create message object and set source and destination field.
    RandomMessage *msg = new RandomMessage(msgname);
    msg->setSource(src);
    msg->setDestination(dest);
    return msg;
}
void RandomNode::forwardMessage(RandomMessage *msg)
{
    // Increment hop count.
    msg->setHopCount(msg->getHopCount()+1);
    // Same routing as before: random gate.
    int n = gateSize("gate");
    int k = intuniform(0, n-1);
    EV << "Forwarding message " << msg << " on gate[" << k << "]\n";
    send(msg, "gate$o", k);
}
